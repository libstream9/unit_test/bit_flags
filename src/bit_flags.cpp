#include <stream9/bit_flags.hpp>

#include <cstdint>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(bit_flags_)

    enum class my_flag : uint32_t {
        none = 0,
        foo = 1,
        bar = 2,
    };

    BOOST_AUTO_TEST_CASE(construct_)
    {
        stream9::bit_flags<my_flag> f;

        BOOST_CHECK(f == my_flag::none);
    }

    BOOST_AUTO_TEST_CASE(and_1_)
    {
        stream9::bit_flags<my_flag> f = my_flag::foo;

        f = f & my_flag::foo;
        BOOST_CHECK(f == my_flag::foo);

        f = f & my_flag::bar;
        BOOST_CHECK(f == my_flag::none);
    }

    BOOST_AUTO_TEST_CASE(and_2_)
    {
        stream9::bit_flags<my_flag> f = my_flag::foo;

        f &= my_flag::foo;
        BOOST_CHECK(f == my_flag::foo);

        f &= my_flag::bar;
        BOOST_CHECK(f == my_flag::none);
    }

    BOOST_AUTO_TEST_CASE(or_1_)
    {
        stream9::bit_flags<my_flag> f;

        f = f | my_flag::foo;
        BOOST_CHECK(f & my_flag::foo);

        f = f | my_flag::bar;
        BOOST_CHECK(f & my_flag::foo);
        BOOST_CHECK(f & my_flag::bar);
    }

    BOOST_AUTO_TEST_CASE(or_2_)
    {
        stream9::bit_flags<my_flag> f;

        f |= my_flag::foo;
        BOOST_CHECK(f == my_flag::foo);

        f |= my_flag::bar;
        BOOST_CHECK(f & my_flag::foo);
        BOOST_CHECK(f & my_flag::bar);
    }

BOOST_AUTO_TEST_SUITE_END() // bit_flags_

} // namespace testing
